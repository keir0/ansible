# Ansible

Ansible roles and configurations

## ToDo list

- Create a generic "docker" role to asbtract compose deployment, handlers, config deployment
- Put container in host networking mode behind traefik ("file config" ? Move them to bridge networking ?)

## Secret Encryption

Secrets are encrypted using Ansible Vault. The vault's password should be stored in ~/.ansible/vault.key with the appropriate permissions.
The secret can also be provided when executing a playbook by adding the '--ask-vault-pass' to ansible-playbook command.

### Creating encrypted variables
	$ ansible-vault encrypt_string  'my_secret_value' --name 'my_secret_variable'

	Encryption successful
			my_secret_variable: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          30306365376530303561336238656434626566396433363837633765356531366432353335383331
          3663616334666139313730653962643962366265323561350a636361646266353764616136613465
          36323262303238306334623733623663383563643562636433653661333834646433663436396630
          3633333934633636610a646133323835316235313936623963666635396664303364663635323335
          6266%

### Reading ecnrypted variables
	echo '$ANSIBLE_VAULT;1.1;AES256                                                                                                                                          INT ✘
	         30306365376530303561336238656434626566396433363837633765356531366432353335383331
	         3663616334666139313730653962643962366265323561350a636361646266353764616136613465
	         36323262303238306334623733623663383563643562636433653661333834646433663436396630
	         3633333934633636610a646133323835316235313936623963666635396664303364663635323335
	         6266'| tr -d ' ' | ansible-vault decrypt --vault-pass-file ~/.ansible/vault.key
