Role Name
=========

This Ansible role installs WireGuard using `wg-easy`, a simple web-based GUI for managing WireGuard VPNs. The role is designed to work with a specific inventory format, allowing for easy configuration of your WireGuard server.

Requirements
------------

Ansible 2.9 or later
Docker and Docker Compose installed on the target machine

Role Variables
--------------

wireguard:
  data_dir: "/path/to/data"                # Directory to store WireGuard data
  host: "vpn.example.com"                   # Hostname for the WireGuard server
  port: 51820                                # The port for WireGuard
  password_hash: "your_password_hash_here"  # Hashed password for wg-easy
  default_address: "192.168.10.x"           # Default address for WireGuard clients
  default_dns: "192.168.1.1"                # DNS server for clients
  allowed_ips: "192.168.1.0/24,192.168.10.0/24" # Allowed IPs for clients

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: wireguard }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
