Role Name
=========

This role installs Home-Assistant and deploys my configuration using docker-compose

Requirements
------------

N/A

Role Variables
--------------
```
ha:
  data_dir: Root directory for docker volume
  covers:  A list of covers entity that will be accesible via airsend plateform
    - name:
      entity_name:
      id:
      type:
      fav_cmd_id:
      apiKey:
      channel:
        id:
        source:
```

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: home-assistant, tags: "home-assistant" }


License
-------

BSD

Author Information
------------------

https://gitlab.com/keir0
