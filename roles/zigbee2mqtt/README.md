Role Name
=========

This role installs Zigbee2Mqtt and deploys my configuration using docker-compose

Requirements
------------

N/A

Role Variables
--------------
```
---
zigbee2mqtt:
  data_dir: Root directory for docker volume
  device: USB zigbee key
  port: port for the frontend
  network_key: zigbee network encryption key
```

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: zigbee2mqtt, tags: "zigbee2mqtt" }


License
-------

BSD

Author Information
------------------

https://gitlab.com/keir0
