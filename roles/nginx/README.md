Role Name
=========

This role manage nginx custom config for my synology server

Requirements
------------

N/A

Role Variables
--------------

- subdomains : contains the list of all the conf file to create (one for each subdomain)

For each subdomains, the following options can be configured : 
- https (defaults to true) 		: enables the https 
- http_port (defaults to 80) 		: self explanatory 
- https_port (defaults to 443)		: self explanatory
- custom_error_pages (defaults to true) : use synology default error pages

- locations : a list of locations entry

each location entry requires at least a name. The following options can be configured
- name: 				: name of the location 
- proxy_pass 				: use the proxy function of nginx to transfer the request
- return 				: returns an http code

  Exemple : 
      subdomains:
	- subdomain: keir0.fr
	    http_port: 88
	    locations:
	      - name: /
	        proxy_pass: https://127.0.0.1:1337
	
	  - subdomain: '*.keir0.fr'
	    locations:
	      - name: /
	        return: 403

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: nginx, tags: nginx }

License
-------

BSD

Author Information
------------------

https://gitlab.com/keir0
